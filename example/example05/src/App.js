import React, {useRef} from 'react';
import './App.css';

const App = () => {
  const fileRef = useRef(null);

  return (
    <div>
      <input type="file" ref={fileRef} style={{display: 'none'}} />
      <button onClick={() => fileRef.current.click()}>Upload</button>
    </div>
  );
}

export default App;
