import React, {useState} from 'react';
import "./App.css";

const Username = ({ value, onChange }) => {
  return (
    <div>
      <label>Username : </label>
      <input type="text" value={value} onChange={onChange} />
    </div>
  );
};

const Password = ({ value, onChange }) => {
  return (
    <div>
      <label>Password : </label>
      <input type="password" value={value} onChange={onChange} />
    </div>
  );
};

const App = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const submit = () => {
    console.log(username, password);
  }

  return (
    <div className="container">
      <Username value={username} onChange={(e) => setUsername(e.target.value)} />
      <br />
      <Password value={password} onChange={(e) => setPassword(e.target.value)} />
      <br />
      <button type="button" onClick={submit}>Submit</button>
    </div>
  );
};

export default App;
