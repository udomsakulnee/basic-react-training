import "./App.css";

const App = () => {
  const lists = [
    {
      name: {
        first: "Alexandre",
        last: "Sanchez",
      },
      gender: "male",
      age: 31,
    },
    {
      name: {
        first: "Layla",
        last: "Anderson",
      },
      gender: "female",
      age: 42,
    },
    {
      name: {
        first: "Elisabeth",
        last: "Stensrud",
      },
      gender: "female",
      age: 43,
    },
    {
      name: {
        first: "Robin",
        last: "Lindelauf",
      },
      gender: "female",
      age: 57,
    },
    {
      name: {
        first: "Isaac",
        last: "Robinson",
      },
      gender: "male",
      age: 61,
    },
  ];

  const filterLists = lists.filter((item) => item.age > 40);

  return (
    <div className="container">
      <h1>List Example</h1>
      {lists.map(({ name: { first, last }, gender, age }, index) => (
        <div key={index}>
          <h2>{`${first} ${last}`}</h2>
          <p>
            Gender: {gender} | Age: {age}
          </p>
        </div>
      ))}
    </div>
  );
};

export default App;
