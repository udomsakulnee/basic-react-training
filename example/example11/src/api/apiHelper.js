import {get} from "lodash";
import api from "./api";

export const getAllUser = async ({length = 100}) => {
    try {
        const response = await api.get(`/?results=${length}`);
        if (response.status === 200) {
            return get(response, "data.results", []);
        }
        else {
            throw new Error();
        }
    }
    catch (err) {
        throw err;
    }
}