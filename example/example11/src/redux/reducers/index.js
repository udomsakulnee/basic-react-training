import { combineReducers } from "redux";
import spinner from "./spinner.reducer";
import popup from "./popup.reducer";

export default combineReducers({
  spinner,
  popup,
});
