import { HIDE_SPINNER, SHOW_SPINNER } from "../constants";

const initialState = {
  isShow: false,
};

const reducer = (state = initialState, { type }) => {
  switch (type) {
    case SHOW_SPINNER:
      return { ...state, isShow: true };

    case HIDE_SPINNER:
      return { ...state, isShow: false };

    default:
      return state;
  }
};

export default reducer;
