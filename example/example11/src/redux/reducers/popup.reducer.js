import { HIDE_POPUP, SHOW_POPUP } from "../constants";

const initialState = {
  isShow: false,
  title: "",
  description: "",
  action: () => {},
};

const reducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_POPUP:
      return { ...state, ...payload, isShow: true };

    case HIDE_POPUP:
      return initialState;

    default:
      return state;
  }
};

export default reducer;
