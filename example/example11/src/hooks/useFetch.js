import api from "../api/api";
import {useState, useEffect} from "react";
import {useDispatch} from "react-redux";
import {showSpinner, hideSpinner} from "../redux/actions";

const useFetch = (url) => {
    const [isLoading, setIsLoading] = useState(false);
    const [isSuccess, setIsSuccess] = useState(true);
    const [data, setData] = useState([]);
    const dispatch = useDispatch();

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            dispatch(showSpinner());
            try {
                const response = await api.get(url);
                if (response.status === 200) {
                    setData(response.data);
                    setIsSuccess(true);
                }
                else {
                    setIsSuccess(false);
                }

                setIsLoading(false);
                dispatch(hideSpinner());
            }
            catch (err) {
                setIsSuccess(false);
                setIsLoading(false);
                dispatch(hideSpinner());
            }
        }

        fetchData();
    }, [dispatch, url]);

    return {
        data,
        isLoading,
        isSuccess,
    }
};

export default useFetch;
