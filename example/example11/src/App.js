import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Spinner from "./components/Spinner/Spinner";
import Popup from "./components/Popup/Popup";
import HomePage from "./pages/Home/Home";
import UserPage from "./pages/User/User";

const App = () => {
  return (
    <BrowserRouter>
      <Spinner />
      <Popup />
      <Switch>
        <Route path="/home" render={(props) => <HomePage {...props} />} />
        <Route path="/user" render={(props) => <UserPage {...props} />} />
        <Redirect from="*" to="/home" />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
