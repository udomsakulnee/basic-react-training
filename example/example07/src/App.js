import React, { useState } from "react";
import { Center, Wrapper } from "./components/shared";
import { Input, Button, ListGroup, ListGroupItem } from "reactstrap";
import styles from "./App.module.css";

const App = () => {
  const [notes, setNotes] = useState([]);
  const [text, setText] = useState("");

  const save = () => {
    if (text !== "") {
      setNotes([...notes, text]);
      setText("");
    }
  };

  return (
    <Center>
      <Wrapper>
        <h3 className={styles.caption}>Note</h3>
        <Input
          type="text"
          placeholder="please input new note..."
          value={text}
          onChange={(e) => setText(e.target.value)}
        />
        <div className="text-center">
          <Button type="text" color="primary" className="mt-3" onClick={save}>
            Save
          </Button>
        </div>
        <ListGroup className="mt-3">
          {notes.map((item, key) => (
            <ListGroupItem key={key}>{item}</ListGroupItem>
          ))}
        </ListGroup>
      </Wrapper>
    </Center>
  );
};

export default App;
