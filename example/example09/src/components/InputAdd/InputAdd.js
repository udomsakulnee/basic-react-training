import React, { useState } from "react";
import { Button, Input } from "reactstrap";
import PropTypes from "prop-types";
import styles from "./InputAdd.module.css";

export const InputAdd = ({ onChange }) => {
  const [text, setText] = useState("");

  const addText = () => {
    if (text !== "") {
      onChange(text);
      setText("");
    }
  };

  return (
    <div className={styles.container}>
      <Input
        type="text"
        className={styles.input}
        value={text}
        onChange={(e) => setText(e.target.value)}
      />
      <Button color="primary" onClick={addText}>
        +
      </Button>
    </div>
  );
};

InputAdd.defaultProps = {
  onChange: () => {},
};

InputAdd.propTypes = {
  onChange: PropTypes.func,
};
