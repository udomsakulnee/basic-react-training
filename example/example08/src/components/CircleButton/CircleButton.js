import React from "react";
import PropTypes from "prop-types";
import styles from "./CircleButton.module.css";

const CircleButton = ({ children, onClick }) => {
  return <h6 className={styles.circle} onClick={onClick}>{children}</h6>;
};

CircleButton.defaultProps = {
  children: "",
  onClick: () => {},
};

CircleButton.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
};

export default CircleButton;
