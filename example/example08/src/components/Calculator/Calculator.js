import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import Display from "../Display/Display";
import CircleButton from "../CircleButton/CircleButton";
import styles from "./Calculator.module.css";

const Calculator = ({ value, onChange }) => {
  const [text, setText] = useState("0");

  useEffect(() => {
    setText(value.toString());
  }, [value]);

  const handleClickNum = (value) => {
    if (text === "0") {
      setText(value);
    } else {
      setText(`${text}${value}`);
    }
  };

  const handleClear = () => {
    setText("0");
  };

  const handleDelete = () => {
    if (text !== "0") {
      if (text.length > 1) {
        setText(text.slice(0, -1));
      } else {
        setText("0");
      }
    }
  };

  const handleClickOK = () => {
    onChange(Number(text));
  };

  return (
    <div className={styles.container}>
      <Display value={text} />
      <div className={styles.button}>
        <CircleButton onClick={() => handleClickNum("1")}>1</CircleButton>
        <CircleButton onClick={() => handleClickNum("2")}>2</CircleButton>
        <CircleButton onClick={() => handleClickNum("3")}>3</CircleButton>
        <CircleButton onClick={() => handleClickNum("4")}>4</CircleButton>
        <CircleButton onClick={() => handleClickNum("5")}>5</CircleButton>
        <CircleButton onClick={() => handleClickNum("6")}>6</CircleButton>
        <CircleButton onClick={() => handleClickNum("7")}>7</CircleButton>
        <CircleButton onClick={() => handleClickNum("8")}>8</CircleButton>
        <CircleButton onClick={() => handleClickNum("9")}>9</CircleButton>
        <CircleButton onClick={handleClear}>C</CircleButton>
        <CircleButton onClick={() => handleClickNum("0")}>0</CircleButton>
        <CircleButton onClick={handleDelete}>&lt;</CircleButton>
      </div>
      <div className="text-center mt-1">
        <Button type="text" color="success" size="sm" onClick={handleClickOK}>
          OK
        </Button>
      </div>
    </div>
  );
};

Calculator.defaultProps = {
  value: 0,
  onChange: () => {},
};

Calculator.propTypes = {
  value: PropTypes.number,
  onChange: PropTypes.func,
};

export default Calculator;
