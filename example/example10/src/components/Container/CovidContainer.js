import React, { useEffect, useState } from "react";
import api from "../../api/api";
import Covid from "../Covid/Covid";

const CovidContrainer = () => {
  const [data, setData] = useState({});

  const fetchData = async () => {
    try {
      const response = await api.get("/api/open/today");
      const { status, data } = response;
      if (status === 200) {
        setData(data);
      }
    } catch (error) {}
  };
  
  useEffect(() => {
    fetchData();
  }, []);

  return <Covid data={data} />;
};

export default CovidContrainer;
