import React from "react";
import PropTypes from "prop-types";
import { Container, Column, Title, Content, Content2 } from "./Card.style";

const Card = ({ title, number, number2, style, backgroundColor }) => {
  return (
    <Container backgroundColor={backgroundColor}>
      <Column>
        <Title>{title}</Title>
        <Content>{number}</Content>
        <Content2>({number2})</Content2>
      </Column>
    </Container>
  );
};

Card.propTypes = {
  backgroundColor: PropTypes.string,
  title: PropTypes.string,
  number: PropTypes.string,
  number2: PropTypes.string,
  style: PropTypes.object,
};

export default Card;
