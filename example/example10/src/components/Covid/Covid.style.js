import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

export const Wrapper = styled.div`
  padding: 40px;
  width: 1175px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
`;

export const Row = styled.div`
  display: flex;
  flex-direction: row;
  flex: 1;
`;

export const Column = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

export const H1 = styled.h1`
  color: #006738;
  margin: 4px;
`;

export const H2 = styled.h2`
  color: #056738;
  margin: 2px;
`;

export const Content = styled.p`
  padding: 12px;
`;