import React, {useState} from 'react';
import './App.css';

const App = () => {
  const [digit, setDigit] = useState([0, 0, 0]);

  const randomNumber = () => {
    return Math.ceil(Math.random() * 9);
  }

  const random = () => {
    setDigit([randomNumber(), randomNumber(), randomNumber()]);
  }

  return (
    <div className="container">
      <h1 className="title">Random Number</h1>
      <div className="digit-container">
        <h2 className="digit">{digit[0]}</h2>
        <h2 className="digit">{digit[1]}</h2>
        <h2 className="digit">{digit[2]}</h2>
      </div>
      <button type="button" onClick={random}>Random</button>
    </div>
  );
}

export default App;
