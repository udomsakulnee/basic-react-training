import React from "react";
import { Loading } from "./Spinner.style";
import { useSelector } from "react-redux";

export const Spinner = () => {
  const { isShow } = useSelector((state) => state.spinner);

  return (
    isShow && <Loading>
      <div class="loader">Loading...</div>
    </Loading>
  );
};
