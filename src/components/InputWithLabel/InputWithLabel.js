import React from "react";
import { Input } from "reactstrap";
import styles from "./InputWithLabel.module.css";
import PropTypes from "prop-types";

// 4 props
// label
// type
// value
// onChange

export const InputWithLabel = ({ label, type, value, onChange }) => {
  return (
    <div>
      <label className={styles.label}>{label} :</label>
      <Input
        className={styles.input}
        type={type}
        value={value}
        onChange={onChange}
      />
    </div>
  );
};

InputWithLabel.defaultProps = {
  label: "",
  type: "text",
  value: "",
  onChange: () => {},
};

InputWithLabel.propTypes = {
  label: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func,
};
