import React, {useState} from "react";

const App = () => {
  const [money, setMoney] = useState(0);
  const [result, setResult] = useState(0);

  const calVat = () => {
    const result = Number(money) * 1.07;
    setResult(result);
  }

  return (
    <div>
      <h1>Vat Calculator</h1>
      <div>
        <p>{money}</p>
        <label>Money: </label>
        <input type="text" value={money} onChange={(e) => {setMoney(e.target.value)}} />
        <button type="button" onClick={calVat}>Calculate</button>
      </div>
      <div>
        <p>Result: {result}</p>
      </div>
    </div>
  );
};

export default App;
