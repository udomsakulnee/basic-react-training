import React, { useState, useEffect } from "react";
import Display from "./Display";
import "./App.css";

const App = () => {
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [toggle, setToggle] = useState(false);

  // let elementDisplay;
  // if (toggle) {
  //   elementDisplay = <Display firstname={firstname} lastname={lastname} />;
  // }

  // update
  useEffect(() => {
    console.log(firstname);
  }, [firstname])

  return (
    <div>
      <div>
        <label>firstname : </label>
        <input
          value={firstname}
          onChange={(e) => {
            setFirstname(e.target.value);
          }}
          type="text"
        />
      </div>
      <br />

      <div>
        <label>lastname : </label>
        <input
          type="text"
          onChange={(e) => {
            setLastname(e.target.value);
          }}
        />
      </div>

      <br />
      <button type="button" onClick={() => setToggle(!toggle)}>
        toggle
      </button>

      {toggle && <Display firstname={firstname} lastname={lastname} />}
      {/* {elementDisplay} */}
    </div>
  );
};

export default App;
