import React, { useEffect } from "react";

const Display = ({ firstname, lastname }) => {
  // mounting, inital
  useEffect(() => {
    console.log("mounting");
  }, []);

  // unmounting
  useEffect(() => {
    return () => {
      console.log("unmounting");
    };
  });

  return <p>{`${firstname} ${lastname}`}</p>;
};

export default Display;
