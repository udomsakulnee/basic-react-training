import React from "react";
import "./App.css";

const App = () => {
  return (
    <div>
      <p className="header text-success">paragraph 1</p>
      <p className="paragraph text-success">paragraph 2</p>
      <p className="paragraph">
        paragraph 3
        <div>
          <span className="span">span1</span>
          <span className="span">span2</span>
        </div>
      </p>
      <span className="span">span3</span>

      <br /><br /><br />
      <div className="box-model">
        Box Model
      </div>
      <div className="box-model">
        Box Model
      </div>

      <div style={{position: 'relative'}}>
        <div className="box">BOX 1</div>
        <div className="box relative">BOX 2</div>
        <div className="box">BOX 3</div>
        <div className="box absolute">BOX 4</div>
        <div className="box">BOX 5</div>
        <div className="box">BOX 6</div>
      </div>

      <div style={{backgroundColor: '#ff0000'}}>
        <span style={{float: 'left'}}>left</span>
        <span style={{float: 'right'}}>right</span>
      </div>
    </div>
  );
};

export default App;
