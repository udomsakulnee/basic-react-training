import React, { useState } from "react";
import styles from "./Login.module.css";
import logo from "../../assets/images/logo.png";
import { Button } from "reactstrap";
import {useHistory} from "react-router-dom";
import {Container, InputWithLabel, Wrapper} from "../../components/shared";
import {useDispatch} from "react-redux";
import {showSpinner} from "../../redux/actions";

const Login = () => {
  const history = useHistory();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();

  const login = () => {
    dispatch(showSpinner());
    // console.log(username, password);
    if (username === "") {
      alert("Username is required");
    }
    else {
      // call api
      history.push("/home");
    } 
  }

  return (
    <Container>
      <Wrapper>
        <div className={styles.logoContainer}>
          <img className={styles.logo} alt="logo" src={logo} />
        </div>
        <h2 className={styles.title}>Login</h2>
        <InputWithLabel
          label="Username"
          value={username}
          onChange={(e) => {
            setUsername(e.target.value);
          }}
        />
        <InputWithLabel
          label="Password"
          type="password"
          value={password}
          onChange={(e) => {
            setPassword(e.target.value);
          }}
        />
        <div className="text-center">
          <Button type="button" color="primary" onClick={login}>
            Submit
          </Button>
        </div>
      </Wrapper>
    </Container>
  );
};

export default Login;
