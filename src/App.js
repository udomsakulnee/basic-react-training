import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import LoginPage from "./pages/Login/Login";
import HomePage from "./pages/Home/Home";
import CovidPage from "./pages/Covid/Covid";
import UserPage from "./pages/User/User";
import UserDetailPage from "./pages/User/UserDetail";
import "./App.css";
import {Spinner, Popup} from "./components/shared";

const App = () => {
  return (
    <BrowserRouter>
      <Spinner />
      <Popup />
      <Switch>
        <Route path="/login" render={(props) => <LoginPage {...props} />} />
        <Route path="/home" render={(props) => <HomePage {...props} />} />
        <Route path="/covid" render={(props) => <CovidPage {...props} />} />
        <Route path="/user" render={(props) => <UserPage {...props} />} />
        <Route path="/user-detail" render={(props) => <UserDetailPage {...props} />} />
        <Redirect from="*" to="/login" />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
