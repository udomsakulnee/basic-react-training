import React from "react";

// const foo = (a) => {
//   return a + 1;
// }
// console.log(foo(5));

// const foo = a => {
//   return a + 1;
// }
// console.log(foo(5));

const foo = a => a + 1;
console.log(foo(5));

const App = () => {
  return <p>Function</p>
}

export default App;
