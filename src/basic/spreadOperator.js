// Spread Operator ...
const obj1 = {a: 1, b: 2,};
const obj2 = {a: 5, c: 3, d: 4,};
const obj3 = {...obj2, e: 10, ...obj1,};

const arr1 = [1, 2, 3];
const arr2 = [4, 5, 6];
const arr3 = [...arr1, ...arr2];
console.log(arr3);

function App() {

  return <p></p>
}

export default App;
