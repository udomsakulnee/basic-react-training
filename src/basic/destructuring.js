// Destructuring Array
const arr = [1, 2, 3];
const [a, b, c] = arr;

// Destructuring Object
const person = {
  age: 29,
  gender: "male",
  height: 100,
  name: {
    firstName: "Wisut",
    lastName: "Udom",
    nickname: "Aun",
  }
}
const {gender, age, height = 0, name: {firstName}} = person;

function App() {
  // expression
  return <p>{height}</p>
}

export default App;
