import React from "react";

// short circuit evaluation
console.log(true && 'text');
console.log(false || 'text');

const App = () => {
  return <p>Function</p>
}

export default App;
