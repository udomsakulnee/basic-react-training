import { SHOW_SPINNER, HIDE_SPINNER } from "../constants";

const initalState = {
  isShow: false,
};

const reducer = (state = initalState, { type }) => {
  switch (type) {
    case SHOW_SPINNER:
      return { ...state, isShow: true };

    case HIDE_SPINNER:
      return { ...state, isShow: false };

    default:
      return state;
  }
};

export default reducer;
